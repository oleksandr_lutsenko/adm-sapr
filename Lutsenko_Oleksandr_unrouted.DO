# ACCEL PCB V19.02.9589 Auto-Generated DO File
# Tue Nov 19 04:27:03 2019
#
bestsave on $\best.w
status_file $\progress.sts
#
unit mm
#
grid wire 0.100000 
grid via 0.100000 
#
rule pcb (width 0.300)
#
bus diagonal
route 50
clean 4
route 50 16
clean 4
filter 5
route 100 16
clean 2
delete conflicts
#
write wire $\Lutsenko_Oleksandr.w
spread 
miter
write wire $\Lutsenko_Oleksandr.m
#
write session $\Lutsenko_Oleksandr.ses
report status $\Lutsenko_Oleksandr.sts
